<?php namespace Medinam\GoogleCaptcha;

use Backend;
use App;
use Validator;
use Event;
use Lang;
use System\Classes\PluginBase;
use Illuminate\Foundation\AliasLoader;
use Medinam\GoogleCaptcha\Classes\GoogleCaptcha;
use Medinam\GoogleCaptcha\Models\Settings as GoogleCaptchaSettings;
use Medinam\GoogleCaptcha\Classes\Facades\GoogleCaptcha as GoogleCaptchaFacade;
use Medinam\GoogleCaptcha\Components\InitGoogleCaptcha;
use Medinam\GoogleCaptcha\Components\GoogleCaptchaField;
use Medinam\GoogleCaptcha\Classes\Rules\GoogleCaptcha as GoogleCaptchaRule;

/**
 * GoogleCaptcha Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Google Captcha',
            'description' => 'Protect your October CMS website using Google reCAPTCHA V2 or V3.',
            'author'      => 'Luis Medina',
            'icon'        => 'icon-key'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $alias = AliasLoader::getInstance();
        $alias->alias('GoogleCaptcha', GoogleCaptchaFacade::class);

        Event::listen('translator.beforeResolve', function ($key, $replaces, $locale) {
            if ($key === 'validation.googlecaptcha') {
                return Lang::get('medinam.googlecaptcha::validation.googlecaptcha');
            }
        });

        App::singleton('medinam.googlecaptcha', function() {
            return new GoogleCaptcha;
        });

        Validator::extendImplicit('googlecaptcha', GoogleCaptchaRule::class);
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            InitGoogleCaptcha::class => 'initGoogleCaptcha',
            GoogleCaptchaField::class => 'googleCaptchaField'
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'medinam.googlecaptcha.access_plugin_settings' => [
                'tab' => 'Google Captcha',
                'label' => 'Manage keys and other plugin settings.'
            ],
        ];
    }

    /**
     * Registers settings
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Google Captcha Settings',
                'description' => 'Manage keys and other plugin settings.',
                'icon'        => 'icon-key',
                'class'       => GoogleCaptchaSettings::class,
                'order'       => 500,
                'category'    => 'Google Captcha',
                'keywords'    => 'google captcha',
                'permissions' => ['medinam.googlecaptcha.access_plugin_settings']
            ],
            'whitelistips' => [
                'label'       => 'Whitelist IPs',
                'description' => 'Hide Google Captcha for the whitelisted IPs.',
                'category'    => 'Google Captcha',
                'icon'        => 'icon-list',
                'url'         => Backend::url('medinam/googlecaptcha/whitelistips'),
                'order'       => 500,
                'keywords'    => 'google captcha',
                'permissions' => ['medinam.googlecaptcha.access_plugin_settings']
            ]
        ];
    }
}
