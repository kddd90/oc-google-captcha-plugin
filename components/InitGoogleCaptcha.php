<?php namespace Medinam\GoogleCaptcha\Components;

use Log;
use Request;
use Cms\Classes\ComponentBase;
use Medinam\GoogleCaptcha\Models\Settings as GoogleCaptchaSettings;
use Medinam\GoogleCaptcha\Models\WhitelistIp;

class InitGoogleCaptcha extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Initialize Google Captcha',
            'description' => 'Load the JavaScript API with your sitekey.'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $recaptchaVersion = GoogleCaptchaSettings::get('recaptcha_version');
        $gcIsActive = GoogleCaptchaSettings::get('is_active', false);
        $ipWhitelisted = WhitelistIp::where('ip', Request::ip())->first();

        if ($gcIsActive && !$ipWhitelisted) {
            if ($recaptchaVersion == 'v3') {
                $this->addJs('https://www.google.com/recaptcha/api.js?render=' .
                        GoogleCaptchaSettings::get('site_key', 'null'));
            }
            else {
                $this->addJs('https://www.google.com/recaptcha/api.js');
            }
        }
    }
}
