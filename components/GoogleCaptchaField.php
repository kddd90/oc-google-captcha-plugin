<?php namespace Medinam\GoogleCaptcha\Components;

use Request;
use Cms\Classes\ComponentBase;
use Medinam\GoogleCaptcha\Models\Settings as GoogleCaptchaSettings;
use Medinam\GoogleCaptcha\Models\WhitelistIp;

class GoogleCaptchaField extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Google Captcha Field',
            'description' => 'Hidden input field with reCAPTCHA V3.'
        ];
    }

    public function defineProperties()
    {
        return [
            'action' => [
                'title' => 'Action',
                'description' => 'A new concept introduced on reCAPTCHA v3 for data analysis.',
                'type' => 'string',
                'default' => 'default',
                'required' => true,
                'validationPattern' => '^[a-zA-Z0-9/]*$',
                'validationMessage' => 'Action may only contain alphanumeric characters and slashes.'
            ]
        ];
    }

    public function onRun()
    {
        $this->addJs('/plugins/medinam/googlecaptcha/assets/js/googlecaptcha.js');
    }

    public function public_key()
    {
        return GoogleCaptchaSettings::get('site_key');
    }

    public function recaptcha_version()
    {
        return GoogleCaptchaSettings::get('recaptcha_version');
    }

    public function gc_is_active()
    {
        return GoogleCaptchaSettings::get('is_active', false);
    }

    public function ip_whitelisted()
    {
        return WhitelistIp::where('ip', Request::ip())->first() ? true : false;
    }

    public function v2_theme()
    {
        return GoogleCaptchaSettings::get('v2_theme', 'light');
    }

    public function v2_size()
    {
        return GoogleCaptchaSettings::get('v2_size', 'normal');
    }
}
