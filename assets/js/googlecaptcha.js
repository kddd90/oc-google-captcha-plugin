'use strict';

function GoogleCaptcha() {

    /**
     * Get the form of any element using a selector or element.
     */
    this.getParentForm = function(selector) {
        return $($(selector).parents('form:first')[0]);
    }

    /**
     * Validate the options.
     */
    this.validateOptions = function(options) {
        if ('version' in options) {
            if (options.version.match(/^(v2|v2i|v3)$/)) {
                return true;
            }
            else {
                console.error('Google Captcha invalid version.');
                return false;
            }
        }
        else {
            console.error('Google Captcha has no version defined.');
            return false;
        }
    }

    /**
     * Load options using data attributes of an element.
     */
    this.loadOptions = function(el) {
        var options = {};

        var version = $(el).data('version');
        var sitekey = $(el).data('sitekey');
        var action = $(el).data('action');

        if (version) {
            options.version = version;
        }
        if (sitekey) {
            options.sitekey = sitekey;
        }
        if (action) {
            options.action = action;
        }

        return options;
    }

    /**
     * V2 auto reload.
     */
    this.v2AutoReload = function(el, options) {
        var form = this.getParentForm(el);

        grecaptcha.ready(function() {
            var widgetID = grecaptcha.render(el, {
                'sitekey' : options.sitekey
            });

            form.on('ajaxAlways', function(event) {
                grecaptcha.reset(widgetID);
            });
        });
    }

    /**
     * V2 Invisible auto reload.
     */
    this.v2InvisibleAutoReload = function(el, options) {
        var form = this.getParentForm(el);
        var formButtons = form.find(':submit');

        grecaptcha.ready(function() {
            var widgetID = grecaptcha.render(el, {
                'sitekey' : options.sitekey,
                'callback' : function() {
                    form.submit();
                    grecaptcha.reset(widgetID);
                },
                'size' : 'invisible'
            });
        });

        formButtons.each(function(index, el) {
            $(el).click(function(event) {
                event.preventDefault()
                grecaptcha.execute();
            });
        });
    }

    /**
     * V3 auto reload.
     */
    this.v3AutoReload = function(el, options) {
        var form = this.getParentForm(el);

        var createGoogleReCaptcha = function() {
            grecaptcha.execute(options.sitekey, {
                action: options.action
            }).then(function(token) {
                $(el).val(token);
            });
        }

        grecaptcha.ready(createGoogleReCaptcha);

        form.on('ajaxAlways', function(el) {
            createGoogleReCaptcha();
        });
    }

    /**
     * Init Google Captcha helper.
     */
    this.init = function(selector) {
        var self = this;
        $(selector).each(function(index, el) {
            var options = self.loadOptions(el);

            if (self.validateOptions(options)) {
                if (options.version == 'v2') {
                    self.v2AutoReload(el, options);
                }
                else if (options.version == 'v2i') {
                    self.v2InvisibleAutoReload(el, options);
                }
                else if (options.version == 'v3') {
                    self.v3AutoReload(el, options);
                }
            }
        });
    }

}

var googleCaptcha = new GoogleCaptcha();
googleCaptcha.init('.lm-google-captcha');
