Google Captcha for October CMS
==============================

This plugin is an effective security solution for your October CMS proyects, allows you to protect you website agains bots in a ease. It can be used with any kind of form like login, registration password recovery, comments and other.

It can be integrated with other plugins using the Google Captcha custom rule, it doesn't care if you wan't to use reCAPTCHA Version 2, Version 2 invisible or the new Version 3.

### Cool features

-   Add Google reCAPTCHA to any kind of forms
-   Hide Google reCAPTCHA for whitelisted IP addresses
-   Change between Google Captcha versions without modify your code
    -   Google reCAPTCHA Version 2 (Invisible included)
    -   Google reCAPTCHA Version 3
-   Refresh automatically reCATPCHA when using October CMS AJAX Framework
-   Simple settings for fast setup
-   Easy to use components
-   Add multiple Google reCATPCHA on the same page

Setup
-----

1.  You need a Google reCAPTCHA API Key, you can get it from [Google reCAPTCHA Admin Console](https://www.google.com/recaptcha/admin).

2.  You need to initialize Google Captcha once on your website using the "Initialize Google Captcha" component, I usually put this component in layouts.

        [initGoogleCaptcha]
        ==

3.  Now you can use the "Google Captcha Field" in forms.


        [googleCaptchaField]
        ==
        <form data-request="onSubmitForm">
            <label>Email address</label>
            <input type="email" name="email">
        
            {% component 'googleCaptchaField' %}
        
            <button type="submit">Submit</button>
        </form>


Validation
----------

You can validate the captcha using the Google Captcha custom rule.

    function onSubmitForm()
    {
        $validator = Validator::make(post(), [
            'g-recaptcha-response' => [
                'googlecaptcha'
            ]
        ]);
    
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

Optionally the Google Captcha rule accepts two parametes: action and score. Action is a new feature added on Google Captcha V3, client action and rule action should match, on the other hand, score is a way to consider valid the response, by default is 0.5. You can found more information on [reCAPTCHA Developer's Guide](https://developers.google.com/recaptcha/intro).

Rule action can be empty only if Google Catchpa Field component use the default action, witch is default.

    function onSubmitForm()
    {
        $validator = Validator::make(post(), [
            'g-recaptcha-response' => [
                'googlecaptcha:login,0.6'
            ]
        ]);
    
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

Extending plugins
-----------------

Is easy extend the validation of other plugin to use Google Captcha custom rule, we can extend the RainLab User plugin by this way:

    use Event;
    use Validator;
    use ValidationException;
    
    function boot()
    {
        Event::listen('rainlab.user.beforeRegister', function(&$data) {
            $validator = Validator::make($data, [
                'g-recaptcha-response' => 'googlecaptcha'
            ]);
    
            if ($validator->fails()) {
                throw new ValidationException($validator);
            }
        });
    }

Found some error?
-----------------

Just go to [plugin's repository page on GitLab](https://gitlab.com/medinam/oc-google-captcha-plugin) and open an issue or propose a merge request.

License
-------

Google Captcha for October CMS is open-source software licensed under the [MIT license](LICENSE).
