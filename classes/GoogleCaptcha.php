<?php namespace Medinam\GoogleCaptcha\Classes;

use Log;
use GuzzleHttp\Client as GuzzleClient;
use Medinam\GoogleCaptcha\Models\Settings as GoogleCaptchaSettings;
use Medinam\GoogleCaptcha\Classes\Exceptions\VerificationException;

class GoogleCaptcha
{
    public static function verify($userToken, $action = null)
    {
        $recaptchaVersion = GoogleCaptchaSettings::get('recaptcha_version');

        if ($recaptchaVersion == 'v3') {
            return self::verifyWithV3($userToken, $action);
        }
        elseif ($recaptchaVersion == 'v2' || $recaptchaVersion == 'v2i') {
            return self::verifyWithV2($userToken);
        }
    }

    protected static function verifyWithV2($userToken) {
        $response = json_decode(self::siteVerify($userToken), true);

        if ($response['success']) {
            return 1.0;
        }
        else {
            throw new VerificationException('Google reCAPTCHA error codes: ' .
                    implode(', ', $response['error-codes']));
        }
    }

    protected static function verifyWithV3($userToken, $action) {
        $response = json_decode(self::siteVerify($userToken), true);

        if ($response['success']) {
            if (strcmp(trim($response['action']), trim($action)) == 0) {
                return $response['score'];
            }
        }
        else {
            throw new VerificationException('Google reCAPTCHA error codes: ' .
                    implode(', ', $response['error-codes']));
        }
    }

    protected static function siteVerify($userToken) {
        $client = new GuzzleClient();
        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $res = $client->request('POST', $url, [
            'form_params' => [
                'secret' => GoogleCaptchaSettings::get("private_key"),
                'response' => $userToken
            ]
        ]);

        return $res->getBody();
    }
}
